#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>


#define ARRAY_SIZE 1000000  // trabalho final com o valores 10.000, 100.000, 1.000.000
int vetor[ARRAY_SIZE];

//Função BubbleSort, responsável pela ordenação do vetor   
void bs(int n, int * vetor) {
    int c=0, d, troca, trocou =1;

    while(c < (n-1) & trocou) {
        trocou = 0;
        for(d = 0 ; d < n - c - 1; d++)
            if(vetor[d] > vetor[d+1]) {
                troca = vetor[d];
                vetor[d] = vetor[d+1];
                vetor[d+1] = troca;
                trocou = 1;
            }
        c++;
   }
}

//Cria o vetor inicial com os valores ordenados decrescentemente(Pior caso)
void inicializa(tam_vetor) {
	int i;

	for(i = 0; i < tam_vetor; i++) {
        vetor[i] = tam_vetor-i;
	}
}

//Função que realiza o merge de duas partes do vetor
void intercala (int p, int q, int r, int v[]) {
  int i, j, k;
  int w[ARRAY_SIZE];

  i = p; 
  j = q;
  k = 0;

  while (i < q && j <= r) {
    if (v[i] <= v[j])  
      w[k++] = v[i++];
    else  
      w[k++] = v[j++];
  }
  
  while (i < q)  
    w[k++] = v[i++];

  while (j <= r)  
    w[k++] = v[j++];

  for (i = p; i <= r; i++)  
    v[i] = w[i-p];
}


void mostraVetor(int *v) {
	int i;
	printf("Algoritmo concluído, vamos imprimir o vetor ordenado!\n");
	for(i = 0; i < ARRAY_SIZE; i++) {
		printf("Posição %d - Elemento: %d\n", i, v[i]);		
	}
}

int main(int argc, char **argv) {
	int tam_vetor, rank;
	int delta = 250000;
	int *vetor_aux;
	int pai;
	int tag = 999;
	double t1, t2;

    MPI_Status stat;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	
	t1 = MPI_Wtime();

	//Recebo vetor e pego o tamanho do mesmo
	if(rank != 0) {
		MPI_Recv(&vetor, ARRAY_SIZE, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
		MPI_Get_count(&stat, MPI_INT, &tam_vetor);
		pai = stat.MPI_SOURCE;
	}
	else {
   		tam_vetor = ARRAY_SIZE;   	//Defino tamanho inicial do vetor
   		inicializa(tam_vetor);     //Sou a raiz e portanto gero o vetor - ordem reversa
   	}

	//Dividir ou conquistar?
	if(tam_vetor <= delta) {
		//Ordena o vetor
		bs(tam_vetor, vetor);
	}
  	else {
		//Envio a metade inicial e a final do vetor para o filho da esquerda e para  da direita
		MPI_Send(&vetor[0], tam_vetor/2, MPI_INT, ((rank*2)+1), tag, MPI_COMM_WORLD);
   	 	MPI_Send(&vetor[tam_vetor/2], tam_vetor/2, MPI_INT, ((rank*2)+2), tag, MPI_COMM_WORLD);
		
		//Receber dos filhos, independente da ordem
		MPI_Recv(&vetor[0], tam_vetor/2, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
		MPI_Recv(&vetor[tam_vetor/2], tam_vetor/2, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);   
		
		//Função que realiza a intercalação das partes dos vetores
		intercala(0, tam_vetor/2, tam_vetor-1, vetor);
	}	

	if(rank != 0) {
		//Caso não seja o processo pai, envia o vetor para o mesmo
		MPI_Send(vetor, tam_vetor, MPI_INT, pai, tag, MPI_COMM_WORLD);
	}
	else {
		//Caso seja o pai, imprime o vetor final, ordenado e intercalado
		mostraVetor(vetor);
	}   
	
	t2 = MPI_Wtime();
	printf("Tempo de demora: %1.2f\n", t2-t1);
    fflush(stdout);

	MPI_Finalize();
	return 0;
}
